package com.dreamcloudz.cloud.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: UserInfoVO.java, v1.0 Exp $
 * @createTime 2021.12.7 11:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoVO implements Serializable {
    private static final long serialVersionUID = -7083462671793914210L;
    private Long id;
    private String name;
    private String code;
    private Integer sex;
}
