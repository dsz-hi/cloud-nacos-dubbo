package com.dreamcloudz.cloud.bean.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: UserInfoDTO.java, v1.0 Exp $
 * @createTime 2021.12.7 11:46
 */
@Data
public class UserInfoDTO implements Serializable {
    private static final long serialVersionUID = 7296330244850057125L;
    private Long id;
    private String name;
    private String code;
}
