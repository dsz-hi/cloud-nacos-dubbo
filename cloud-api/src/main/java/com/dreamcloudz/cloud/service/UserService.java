package com.dreamcloudz.cloud.service;

import com.dreamcloudz.cloud.bean.vo.UserInfoVO;

/**
 * 用户操作服务
 *
 * @author dsz
 * @version $Id: UserService.java, v1.0 Exp $
 * @createTime 2021.12.6 17:13
 */
public interface UserService {
    /**
     * 根据用户id，获取用户详情
     *
     * @param id 用户id
     * @return
     */
    UserInfoVO getUserDetail(Integer id);
}
