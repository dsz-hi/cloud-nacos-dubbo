package com.dreamcloudz.cloud.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: Status.java, v1.0 Exp $
 * @createTime 2021.12.7 14:17
 */
@Data
@NoArgsConstructor
public class Status implements Serializable {
    private static final long serialVersionUID = 8990269767982562992L;

    private Integer code = 0;
    private String des = "";

    public Status(Integer code) {
        this.code = code;
    }

    public Status(Integer code, String msg) {
        this.code = code;
        this.des = msg;
    }
}
