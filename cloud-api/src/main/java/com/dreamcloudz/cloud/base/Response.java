package com.dreamcloudz.cloud.base;

import lombok.Data;

import java.io.Serializable;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: Response.java, v1.0 Exp $
 * @createTime 2021.12.7 14:16
 */
@Data
public class Response<T> implements Serializable {
    private static final long serialVersionUID = -1779268531520096792L;

    private Status status;
    private T data;


    public Response() {
    }

    public Response(Builder<T> builder) {
        this.setStatus(builder.status);
        this.setData(builder.data);
    }

    public boolean isSuccess() {
        return this.status != null && this.status.getCode() == 0;
    }

    public static <T> Response<T> success(T data) {
        return builder().success().data(data).build();
    }

    public static <T> Response<T> success() {
        return success(null);
    }

    public static <T> Response<T> fail(int code, String message) {
        return builder().fail(code, message).build();
    }

    public static <T> Response<T> fail(int code, String message, T data) {
        return builder().fail(code, message).data(data).build();
    }

    public static <T> Builder builder() {
        return new Builder();
    }

    public static final class Builder<T> {
        private Status status;
        private T data;

        public Builder() {
        }

        public Builder<T> status(Status status) {
            this.status = status;
            return this;
        }

        public Builder<T> success() {
            if (this.status != null) {
                this.status.setCode(0);
            } else {
                this.status = new Status(0);
            }
            return this;
        }

        public Builder<T> fail(int code, String desc) {
            if (this.status == null) {
                this.status.setCode(code);
            }
            this.status.setDes(desc);
            return this;
        }

        public Builder<T> code(int code) {

            this.status = new Status(code);
            return this;
        }

        public Builder<T> data(T val) {

            this.data = val;
            return this;
        }

        public Response<T> build() {

            return new Response(this);
        }
    }
}
