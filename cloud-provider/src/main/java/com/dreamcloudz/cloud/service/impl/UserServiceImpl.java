package com.dreamcloudz.cloud.service.impl;

import com.dreamcloudz.cloud.bean.vo.UserInfoVO;
import com.dreamcloudz.cloud.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: UserServiceImpl.java, v1.0 Exp $
 * @createTime 2021.12.7 11:13
 */
@Slf4j
@DubboService
public class UserServiceImpl implements UserService {

    @Override
    public UserInfoVO getUserDetail(Integer id) {
        log.info("getUserDetail userId = {}", id);
        UserInfoVO result = null;
        if (id != null) {
            result = new UserInfoVO();
            result.setId(Integer.toUnsignedLong(id));
            result.setName("Jack");
            result.setCode("code_" + id);
            result.setSex(1);
        }
        log.info("getUserDetail userInfo = {}", result);
        return result;
    }
}
