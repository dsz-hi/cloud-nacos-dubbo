package com.dreamcloudz.cloud.controller;

import com.dreamcloudz.cloud.base.Response;
import com.dreamcloudz.cloud.bean.vo.UserInfoVO;
import com.dreamcloudz.cloud.service.UserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 *    描述信息
 * </pre>
 *
 * @author dsz
 * @version $Id: UserController.java, v1.0 Exp $
 * @createTime 2021.12.7 14:20
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @DubboReference
    UserService userService;

    @GetMapping("/getDetail")
    public Response<UserInfoVO> getDetail(@RequestParam Integer id) {
        UserInfoVO data = userService.getUserDetail(id);
        if (data == null) {
            return Response.fail(-1, "未找到有效用户");
        }
        return Response.success(data);
    }
}
